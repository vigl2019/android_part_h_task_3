package com.example.android_part_h_task_3;

import com.example.android_part_h_task_3.model.RequestModel;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

public class ApiService {

    // https://restcountries.eu/
    // https://restcountries.eu/rest/v2/all

/*
    https://restcountries.eu/rest/v2/currency/{currency}
    https://restcountries.eu/rest/v2/currency/cop
*/

    private static final String API = "https://restcountries.eu/rest/v2/";
    private static PrivateApi privateApi;

    public interface PrivateApi {

        @GET("all")
        Observable<List<RequestModel>> getCountries();

        @GET("capital/{capital}")
        Observable<List<RequestModel>> getCountryByCapital(@Path("capital") String capital);

        @GET("name/{name}")
        Observable<List<RequestModel>> getCountryByName(@Path("name") String name);

        @GET("alpha/{alpha}")
        Observable<RequestModel> getCountryByCode(@Path("alpha") String countryCode);

        @GET("currency/{currency}")
        Observable<List<RequestModel>> getCountriesByCurrencyCode(@Path("currency") String currency);
    }

    static {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(API)
                .client(client)
                .build();

        privateApi = retrofit.create(PrivateApi.class);
    }

    public static Observable<List<RequestModel>> getCountries(){
        return privateApi.getCountries();
    }

    public static Observable<List<RequestModel>> getCountryByCapital(String capital){
        return privateApi.getCountryByCapital(capital);
    }

    public static Observable<List<RequestModel>> getCountryByName(String name){
        return privateApi.getCountryByName(name);
    }

    public static Observable<RequestModel> getCountryByCode(String countryCode){
        return privateApi.getCountryByCode(countryCode);
    }

    public static Observable<List<RequestModel>> getCountriesByCurrencyCode(String currency){
        return privateApi.getCountriesByCurrencyCode(currency);
    }
}


