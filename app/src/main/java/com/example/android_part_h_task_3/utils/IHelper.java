package com.example.android_part_h_task_3.utils;

import com.example.android_part_h_task_3.model.Country;

import java.util.List;

public interface IHelper {

    void getCountries(List<Country> countries);
}