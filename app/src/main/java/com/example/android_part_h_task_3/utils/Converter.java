package com.example.android_part_h_task_3.utils;

import com.example.android_part_h_task_3.model.Country;
import com.example.android_part_h_task_3.model.RequestModel;

import java.util.ArrayList;
import java.util.List;

public class Converter {

    public static List<Country> convert(List<RequestModel> requestModels) {

        List<Country> countries = new ArrayList<>();

        for (int i = 0; i < requestModels.size(); i++) {

            int population = Integer.parseInt(requestModels.get(i).getPopulation());

            countries.add(new Country(i + 1, requestModels.get(i).getName(), population, requestModels.get(i).getArea(), requestModels.get(i).getRegion()));
        }
        return countries;
    }
}

