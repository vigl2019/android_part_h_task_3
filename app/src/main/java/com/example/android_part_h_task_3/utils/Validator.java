package com.example.android_part_h_task_3.utils;

import android.content.Context;

import com.example.android_part_h_task_3.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {

    public static String checkTextFieldIsEmpty(Context context, String value) {
        if (value.isEmpty())
            return context.getResources().getString(R.string.empty_text_field);
        else
            return "";
    }

    public static String checkIntegerValueIsPositive(Context context, String value) {

        int intValue = 0;
        String exception = "";

        try {
            intValue = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            exception = e.getMessage();
        }

        if (intValue <= 0 || !exception.isEmpty())
            return context.getResources().getString(R.string.error_integer_value);
        else
            return "";
    }

    public static String checkFloatValueIsPositive(Context context, String value) {

        float floatValue = 0;
        String exception = "";

        try {
            floatValue = Float.parseFloat(value);
        } catch (NumberFormatException e) {
            exception = e.getMessage();
        }

        if (floatValue <= 0 || !exception.isEmpty())
            return context.getResources().getString(R.string.error_float_value);
        else
            return "";
    }

    public static String checkTextValueIsValid(Context context, String value) {

        String regex = "[\\^a-zA-Z\\$]";
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);

        Matcher matcher = pattern.matcher(value.trim());

        if (matcher.find())
            return "";
        else
            return context.getResources().getString(R.string.error_text_value);
    }

    //================================================================================//

    public static boolean checkField(String value, Context context, String valueType){

        switch(valueType){

            case IValueTypes.INTEGER_VALUE : {

                String errorMessage = Validator.checkTextFieldIsEmpty(context, value);

                if (!errorMessage.isEmpty()) {
                    Helper.createAlertDialog(errorMessage, null, context);

                    return false;
                }

                errorMessage = Validator.checkIntegerValueIsPositive(context, value);

                if (!errorMessage.isEmpty()) {
                    Helper.createAlertDialog(errorMessage, null, context);

                    return false;
                }

                break;
            }
            case IValueTypes.FLOAT_VALUE : {

                String errorMessage = Validator.checkTextFieldIsEmpty(context, value);

                if (!errorMessage.isEmpty()) {
                    Helper.createAlertDialog(errorMessage, null, context);

                    return false;
                }

                errorMessage = Validator.checkFloatValueIsPositive(context, value);

                if (!errorMessage.isEmpty()) {
                    Helper.createAlertDialog(errorMessage, null, context);

                    return false;
                }

                break;
            }
            case IValueTypes.STRING_VALUE : {

                String errorMessage = Validator.checkTextFieldIsEmpty(context, value);

                if (!errorMessage.isEmpty()) {
                    Helper.createAlertDialog(errorMessage, null, context);

                    return false;
                }

                errorMessage = Validator.checkTextValueIsValid(context, value);

                if (!errorMessage.isEmpty()) {
                    Helper.createAlertDialog(errorMessage, null, context);

                    return false;
                }

                break;
            }
            default:
                break;

        }

        return true;
    }
}


