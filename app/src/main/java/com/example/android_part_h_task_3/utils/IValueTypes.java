package com.example.android_part_h_task_3.utils;

public interface IValueTypes {
    String INTEGER_VALUE = "integer";
    String FLOAT_VALUE = "float";
    String STRING_VALUE = "string";
}
