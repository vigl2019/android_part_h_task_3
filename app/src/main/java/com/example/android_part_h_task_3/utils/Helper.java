package com.example.android_part_h_task_3.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.room.Room;

import com.example.android_part_h_task_3.ApiService;
import com.example.android_part_h_task_3.CountriesDataBase;
import com.example.android_part_h_task_3.R;
import com.example.android_part_h_task_3.model.Country;
import com.example.android_part_h_task_3.model.RequestModel;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;

public class Helper {

    private Context context;

    Disposable disposable;
    private CompositeDisposable disposables = new CompositeDisposable();
    private CountriesDataBase dataBase;

//  private IHelper iHelper;

/*
    public Helper(IHelper iHelper){
        this.iHelper = iHelper;
    }
*/

    public Helper(Context context) {
        this.context = context;
    }

    //================================================================================//

    public void getInfoFromServer() {

        dataBase = Room.databaseBuilder(context, CountriesDataBase.class, "CountryDataBase")
//              .allowMainThreadQueries()  // позволяет делать запросы к БД в главном потоке
                .fallbackToDestructiveMigration() // удаляет все старые таблицы и создает новые
                .build();

        //================================================================================//

        disposable = ApiService.getCountries()
                .map(new Function<List<RequestModel>, List<Country>>() {
                    @Override
                    public List<Country> apply(List<RequestModel> requestModels) {

                        List<Country> countries = Converter.convert(requestModels);
                        dataBase.getCountryDao().updateAll(countries);
                        return countries;

                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())

                .filter(new Predicate<List<Country>>() {
                    @Override
                    public boolean test(List<Country> countries) throws Exception {

                        boolean isCountriesListHaveValues = ((countries != null) && (!countries.isEmpty()));

                        if (!isCountriesListHaveValues) {
                            createAlertDialog(context.getResources().getString(R.string.empty_countries_list), null, context);
                        }

                        return isCountriesListHaveValues;
                    }
                })

                .subscribe(new Consumer<List<Country>>() {
                    @Override
                    public void accept(List<Country> list) throws Exception {
                        /* DO NOTHING */
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(context, throwable.getMessage(), Toast.LENGTH_LONG).show();
//                        Toast.makeText(context, "Something went wrong", Toast.LENGTH_LONG).show();
                    }
                });
    }

//    private void generateMethod() {
//        Single.fromCallable(new Callable<Boolean>() {
//            @Override
//            public Boolean call() throws Exception {
//                List<Country> list = new ArrayList<>();
//                list.add(new Country(1, "Ukraine", "Europa"));
//                list.add(new Country(2, "OAE", "Asia"));
//
//                dataBase.getCountryDao().removeAll();
//                dataBase.getCountryDao().insertAll(list);
//                return true;
//            }
//        }).observeOn(AndroidSchedulers.mainThread())
//                .subscribeOn(Schedulers.io())
//                .subscribe();
//    }

//    private void printCountries() {
//        disposables.add(dataBase.getCountryDao().selectAll()
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribeOn(Schedulers.io())
//                .subscribe(new Consumer<List<Country>>() {
//                    @Override
//                    public void accept(List<Country> countries) throws Exception {
//                        for (Country country : countries) {
//                            Log.d("data", country.getName());
//                        }
//                    }
//                }));
//    }

    public void getCountriesByPopulationFromDataBase(int minNumber, int maxNumber, final IHelper iHelper) {

        disposables.add(dataBase.getCountryDao().selectCountriesByPopulation(minNumber, maxNumber)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())

                .filter(new Predicate<List<Country>>() {
                    @Override
                    public boolean test(List<Country> countries) throws Exception {

                        boolean isCountriesListHaveValues = ((countries != null) && (!countries.isEmpty()));

                        if (!isCountriesListHaveValues) {
                            createAlertDialog(context.getResources().getString(R.string.empty_countries_list), null, context);
                        }

                        return isCountriesListHaveValues;
                    }
                })

                .subscribe(new Consumer<List<Country>>() {
                    @Override
                    public void accept(List<Country> countries) throws Exception {
                        iHelper.getCountries(countries);
                    }
                }));
    }

    public void getCountriesByAreaFromDataBase(float minNumber, float maxNumber, final IHelper iHelper) {

        disposables.add(dataBase.getCountryDao().selectCountriesByArea(minNumber, maxNumber)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())

                .filter(new Predicate<List<Country>>() {
                    @Override
                    public boolean test(List<Country> countries) throws Exception {

                        boolean isCountriesListHaveValues = ((countries != null) && (!countries.isEmpty()));

                        if (!isCountriesListHaveValues) {
                            createAlertDialog(context.getResources().getString(R.string.empty_countries_list), null, context);
                        }

                        return isCountriesListHaveValues;
                    }
                })

                .subscribe(new Consumer<List<Country>>() {
                    @Override
                    public void accept(List<Country> countries) throws Exception {
                        iHelper.getCountries(countries);
                    }
                }));
    }

    public void getCountriesByRegionFromDataBase(String region, final IHelper iHelper) {

        disposables.add(dataBase.getCountryDao().selectCountriesByRegion(region)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())

                .filter(new Predicate<List<Country>>() {
                    @Override
                    public boolean test(List<Country> countries) throws Exception {

                        boolean isCountriesListHaveValues = ((countries != null) && (!countries.isEmpty()));

                        if (!isCountriesListHaveValues) {
                            createAlertDialog(context.getResources().getString(R.string.empty_countries_list), null, context);
                        }

                        return isCountriesListHaveValues;
                    }
                })

                .subscribe(new Consumer<List<Country>>() {
                    @Override
                    public void accept(List<Country> countries) throws Exception {
                        iHelper.getCountries(countries);
                    }
                }));
    }

    //================================================================================//

    public void deleteDataBase(Context context, String dataBaseName){
        context.deleteDatabase(dataBaseName);
    }

    //================================================================================//

    public void unSubscribe() {

        if (disposable != null && disposable.isDisposed()) {
            disposable.dispose();
        }

        disposables.clear();
    }

    //================================================================================//

    public static void createAlertDialog(String errorMassage, final View view, Context context) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle("Error of input data!")
                .setMessage(errorMassage)
                .setCancelable(false)
                .setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();

                        if (view != null) {
                            view.requestFocus();
                        }
                    }
                });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }
}



