package com.example.android_part_h_task_3;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.android_part_h_task_3.model.Country;

import java.util.List;

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.CountryViewHolder> {

    Context context;
    List<Country> countries;

    public CountryAdapter(Context context, List<Country> countries) {
        this.context = context;
        this.countries = countries;
    }

    @NonNull
    @Override
    public CountryAdapter.CountryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View countryView = inflater.inflate(R.layout.country_item, parent, false);
        CountryViewHolder countryViewHolder = new CountryViewHolder(countryView);

        return countryViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CountryAdapter.CountryViewHolder holder, int position) {

        Country country = countries.get(position);
        TextView countryName = holder.countryName;
        countryName.setText(country.getName());
    }

    @Override
    public int getItemCount() {
        return countries.size();
    }

    //================================================================================//

    class CountryViewHolder extends RecyclerView.ViewHolder {

        TextView countryName;

        public CountryViewHolder(@NonNull View itemView) {
            super(itemView);

            countryName = itemView.findViewById(R.id.ci_country_name);
        }
    }
}
