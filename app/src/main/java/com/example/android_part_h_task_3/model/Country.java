package com.example.android_part_h_task_3.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Country {

    // поля public можно использовать,
    // когда создаются модели для JSON
    // или для БД

    @PrimaryKey
    private int id;

    private String name;
    private int population;
    private float area;
    private String region;

    // должен быть обязательно пустой конструктор,
    // которым пользуется Room (если нет другого конструктора)
    //  public Country(){}

    public Country(int id, String name, int population, float area, String region) {
        this.id = id;
        this.name = name;
        this.population = population;
        this.area = area;
        this.region = region;
    }

    //================================================================================//

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public float getArea() {
        return area;
    }

    public void setArea(float area) {
        this.area = area;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}



