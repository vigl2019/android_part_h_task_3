package com.example.android_part_h_task_3;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.android_part_h_task_3.model.Country;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public abstract class CountryDao {

    @Insert
    public abstract void insertAll(List<Country> countries);

    // можно в основном потоке или использовать RxJava (работа в отдельном потоке)
    //  @Query("SELECT * FROM Country")
    //  public abstract List<Country> selectAll();

    @Query("SELECT * FROM Country")
    public abstract Flowable<List<Country>> selectAll(); // для Flowable можно добавить буферизацию

    @Query("SELECT * FROM Country WHERE population > :minNumber AND population < :maxNumber")
    public abstract Flowable<List<Country>> selectCountriesByPopulation(int minNumber, int maxNumber);

    @Query("SELECT * FROM Country WHERE area > :minNumber AND area < :maxNumber")
    public abstract Flowable<List<Country>> selectCountriesByArea(float minNumber, float maxNumber);

    @Query("SELECT * FROM Country WHERE region LIKE '%' || :region || '%'")
    public abstract Flowable<List<Country>> selectCountriesByRegion(String region);

    @Query("DELETE FROM Country")
    public abstract void removeAll();

    public void updateAll(List<Country> countries) {
        removeAll();
        insertAll(countries);
    }
}
