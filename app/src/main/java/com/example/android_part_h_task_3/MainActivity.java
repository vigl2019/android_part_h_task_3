package com.example.android_part_h_task_3;

/*

1. Используя API сервиса https://restcountries.eu/ создать приложение с одним экраном.

Приложение должно содержать список стран, поле для ввода данных и кнопку фильтрации.

В текстовое поле пользователь вводит число, нажимает кнопку,
в списке появляются страны, у которых население соответствует указанному числу +/-15%.

При старте приложения должен посылаться запрос на получение данных с сервера и их сохранение в локальную DB.

Все фильтрации происходят с данными в локальной базе.

1.1 Добавить к предыдущему приложению кнопку для фильтрации по площади страны.

1.2* Добавить к предыдущему приложению кнопку для фильтрации по региону.

*/


import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_part_h_task_3.model.Country;
import com.example.android_part_h_task_3.utils.Helper;
import com.example.android_part_h_task_3.utils.IHelper;
import com.example.android_part_h_task_3.utils.IValueTypes;
import com.example.android_part_h_task_3.utils.Validator;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    Helper helper;

    CountryAdapter countryAdapter;

    @BindView(R.id.am_input_value)
    EditText inputValue;

    @BindView(R.id.am_action_get_countries_list_by_population)
    Button actionGetCountriesListByPopulation;

    @BindView(R.id.am_action_get_countries_list_by_area)
    Button actionGetCountriesListByArea;

    @BindView(R.id.am_action_get_countries_list_by_region)
    Button actionGetCountriesListByRegion;

    @BindView(R.id.am_country_list)
    RecyclerView recyclerView;

    private IHelper iHelper = new IHelper() {
        @Override
        public void getCountries(List<Country> countries) {
            countryAdapter = new CountryAdapter(MainActivity.this, countries);
            recyclerView.setAdapter(countryAdapter);
        }
    };

    //================================================================================//

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();

        actionGetCountriesListByPopulation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String value = inputValue.getText().toString().trim();

                if(!Validator.checkField(value, MainActivity.this, IValueTypes.INTEGER_VALUE)){
                    return;
                }

                //==================================================//

                int population = Integer.parseInt(value);

                int minNumber = population - ((population * 15) / 100);
                int maxNumber = population + ((population * 15) / 100);

                helper.getCountriesByPopulationFromDataBase(minNumber, maxNumber, iHelper); // Get info from database
            }
        });

        actionGetCountriesListByArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String value = inputValue.getText().toString().trim();

                if(!Validator.checkField(value, MainActivity.this, IValueTypes.FLOAT_VALUE)){
                    return;
                }

                //==================================================//

                    float area = Float.parseFloat(value);

                    float minNumber = area - ((area * 15) / 100);
                    float maxNumber = area + ((area * 15) / 100);

                    helper.getCountriesByAreaFromDataBase(minNumber, maxNumber, iHelper); // Get info from database
            }
        });

        actionGetCountriesListByRegion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String value = inputValue.getText().toString().trim();

                if(!Validator.checkField(value, MainActivity.this, IValueTypes.STRING_VALUE)){
                    return;
                }

                //==================================================//

                helper.getCountriesByRegionFromDataBase(value, iHelper);
            }
        });
    }

    //================================================================================//

    private void init(){
        ButterKnife.bind(this);
        inputValue.requestFocus();

        helper = new Helper(MainActivity.this);
        helper.deleteDataBase(MainActivity.this, "CountryDataBase");
        helper.getInfoFromServer(); // Get info from server
    }

    @Override
    protected void onStop() {
        super.onStop();

        Helper helper = new Helper(MainActivity.this);
        helper.unSubscribe();
    }
}
